import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './page/admin/admin.component';
import { IndexComponent } from './page/index/index.component';
import { AuthGuard } from './guards/auth.guard';

@NgModule({
  declarations: [AppComponent, AdminComponent, IndexComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,

  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule { }
