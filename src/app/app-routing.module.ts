import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { AdminComponent } from './page/admin/admin.component';
import { IndexComponent } from './page/index/index.component';

const routes: Routes = [
  {
    path: 'MM',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dash',
        loadChildren: () => import('../app/modules/dashboard/dashboard.module').then(x => x.DashboardModule)
      }
    ]
  },
  {
    path: 'index',
    component: IndexComponent,
  },
  {
    path: '**',
    redirectTo: 'index',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
