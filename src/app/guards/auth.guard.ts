import { Injectable } from '@angular/core';
import { ActivatedRoute, CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
  ) { }

  canActivate(): boolean {
    // if (this.authentication.LoggedIn() == true) {
    //   return true;
    // } else {
    //   this.router.navigate(['/auth']);
    //   return false;
    // }
    this.router.navigate(['index']);
    return false;
  }
}
