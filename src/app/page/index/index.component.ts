import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})

export class IndexComponent implements OnInit {

  public innerWidth: any;
  MobileSize = false;
  toggle: any;
  constructor() { }

  ngOnInit() {
    this.toggle = false;
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 768) {
      this.MobileSize = true;
    }
    if (this.innerWidth > 768) {
      this.MobileSize = false;
    }
  }

  toggleshow() {
    this.toggle = !this.toggle;
  }

  scroll(el: HTMLElement): any {
    el.scrollIntoView({ behavior: 'smooth' });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any): any {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 768) {
      this.MobileSize = true;
    }
    if (this.innerWidth > 768) {
      this.MobileSize = false;
    }
  }
}

