const express = require("express");
const path = require("path");
const app = express();
const cors = require('cors');
var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(cors());
app.use(express.static(__dirname + "/dist/mm"));

app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname + "/dist/mm/index.html"));
});

app.listen(process.env.PORT || 8080, () => {
  console.log('Server run on path ', path.dirname(__filename));
});
